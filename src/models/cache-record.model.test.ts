import { CacheRecord } from '~/models/cache-record.model';

jest.useFakeTimers();

describe('cache-record.model', () => {
	const contents = 'something';
	// eslint-disable-next-line @typescript-eslint/init-declarations
	let record: CacheRecord<string>;

	beforeEach(() => {
		record = new CacheRecord(contents);
	});

	test('Contents correct', () => {
		expect(record.contents).toStrictEqual(contents);
	});

	test('Created, modified, read initialized to same date', () => {
		const { created, modified, read } = record;

		expect(modified).toStrictEqual(created);
		expect(read).toStrictEqual(created);
	});

	test('Reading updates read, does not update modified, created', () => {
		const {
			created: originalCreated,
			modified: originalModified,
			read: originalRead
		} = record;

		jest.advanceTimersByTime(100);

		// eslint-disable-next-line @typescript-eslint/no-unused-vars
		const { contents: unused } = record;

		const { created, modified, read } = record;

		expect(created).toStrictEqual(originalCreated);
		expect(modified).toStrictEqual(originalModified);
		expect(read).not.toStrictEqual(originalRead);
		expect(read.getTime()).toBeGreaterThan(originalRead.getTime());
	});

	test('Modifying updates read, modified, does not update created', () => {
		const {
			created: originalCreated,
			modified: originalModified,
			read: originalRead
		} = record;

		jest.advanceTimersByTime(100);

		const newContents = 'other thing';

		record.contents = newContents;

		const { created, modified, read } = record;

		expect(created).toStrictEqual(originalCreated);
		expect(modified).not.toStrictEqual(originalModified);
		expect(modified.getTime()).toBeGreaterThan(originalModified.getTime());
		expect(read).not.toStrictEqual(originalRead);
		expect(read.getTime()).toBeGreaterThan(originalRead.getTime());
	});
});
