import type { CacheAlgorithm } from '~/models/cache-algorithm.model';

export type ICacheOptions = {
	algorithm: CacheAlgorithm;
	maxSize: number;
	ttl: number;
};
